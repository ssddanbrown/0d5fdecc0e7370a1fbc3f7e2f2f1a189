# This sets the name 'bookstack' to point locally on port 3002
# and is used on line 19 below.
upstream bookstack { server 127.0.0.1:3002; }

# This is the main server setup that users access
# If you have existing sites and you are adding bookstack on a
# 'subdirectory' you may have to combine this with your existing configuration.
server {
  listen 80;
  # If copying this whole section you will have to change this to your domain or IP.
  server_name bookstack.dev;
  
  # This is the piece you may have to copy into your existing configration.
  # This block is routing requests to 'http://example.com/docs' to the BookStack
  # server instance set up on port 3002.
  # To change the path change instances of '/docs/' to your intended path, relative to root. 
  location  /docs/ {
    rewrite           ^/docs/(.*) /$1 break;
    proxy_pass  http://bookstack/;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
    proxy_set_header Host $http_host;
    proxy_set_header X-Forwarded-For $remote_addr;
  }
  
}

# This server blocks sets up BookStack on port 3002
# which is then used above. 
server {
    listen 3002;
    listen [::]:3002;

    # Change this to the point to the path of the 'public' folder
    # inside your BookStack instance.
    root /var/www/bookstack/public;
    
    index index.php;

    # Change this to your domain or ip.
    server_name bookstack.dev;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    # Enables PHP files, Set up for php7 on ubuntu.
    location ~ \.php$ {
        try_files $uri /index.php =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/run/php/php7.0-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }
}